package com.syatyo.HomeHack;

import android.app.Activity;
import android.content.ClipData;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HomeHackActivity extends Activity {

    // TAG
    private final String TAG = "HomeHack";

    // WebView
    private WebView mWebView;

    // Settings
    private MenuItem actionSettings;

    // アプリのTOPページ
    private final String APP_PATH = "";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mWebView = (WebView) findViewById(R.id.activity_main_webview);

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new WebViewClient());

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        // Use remote resource
        mWebView.loadUrl("file:///android_asset/index.html");

        // Stop local links and redirects from opening in browser instead of WebView
        // mWebView.setWebViewClient(new HomeHackWebViewClient());
    }

    // Prevent the back-button from closing the app
    @Override
    public void onBackPressed() {
        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

}
